---
layout: handbook-page-toc
title: "Business Systems Documentation"
description: "Business Systems Lead to Fulfillment Documentation"
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Quote to Cash Introduction
The quote-to-cash (QTC) process encompasses lead conversion, customer account management, order fulfillment, billing, and accounts receivables functions. This process is owned by the Enterprise Applications team and this handbook page is intended to be used as a table of contents for key documentation of this process.

## Lead to Fulfillment Applications

<div class="limit-width">
<div class="grid-container">
<div class="grid-item-large" style="grid-row-start: 1;grid-row-end: 2; grid-column-start: 1;grid-column-end: 2;background-color: #c4c4c4"><b>Business Process</b></div>
<div class="grid-item-small" style="grid-column-start: 2;grid-column-end: 4;background-color: #c4c4c4"><b>Supporting System</b></div>
<div class="grid-item-small" style="grid-column-start: 4;grid-column-end: 5;background-color: #c4c4c4"><b>Supporting Team</b></div>
<div class="grid-item-large" style="grid-row-start: 2;grid-row-end: 4; grid-column-start: 1;grid-column-end: 2;background-color: #fdbc60"><b>Market</b></div>
<div class="grid-item-small" style="grid-column-start: 2;grid-column-end: 4;background-color: #fca121">Demand Gen Effort (Marketo)</div>
<div class="grid-item-small" style="grid-column-start: 4;grid-column-end: 5;background-color: #fc9403">Marketing Ops</div>
<div class="grid-item-small" style="grid-column-start: 2;grid-column-end: 4;background-color: #fca121">Trial (Home Grown Application - Ruby on Rails)</div>
<div class="grid-item-small" style="grid-column-start: 4;grid-column-end: 5;background-color: #fc9403">Fulfillment Engineers</div>
<div class="grid-item-empty" style="grid-column-start: 1;grid-column-end: 5;grid-row-start: 4;grid-row-end: 5;"><i class="fas fa-arrow-down"></i></div>
<div class="grid-item-large" style="grid-row-start: 5;grid-row-end: 8;grid-column-start: 1;grid-column-end: 2;background-color: #f2b4a9"><b>Sell</b></div>
<div class="grid-item-small" style="background-color: #e67664; grid-column-start: 2;grid-column-end: 3;">Digital Purchase Path</div>
<div class="grid-item-small" style="background-color: #e67664; grid-column-start: 3;grid-column-end: 4;">Order Form Purchase Path</div>
<div class="grid-item-small" style="background-color: #e05842; grid-column-start: 4;grid-column-end: 5;">Sales Ops/ Deal Desk</div>
<div class="grid-item-small" style="background-color: #e67664; grid-column-start: 2;grid-column-end: 3;">CustomerDot</div>
<div class="grid-item-small" style="background-color: #e67664; grid-column-start: 3;grid-column-end: 4;">Sales Cycle (SFDC)</div>
<div class="grid-item-small" style="background-color: #e05842; grid-column-start: 4;grid-column-end: 5;">Sales Systems</div>
<div class="grid-item-empty" style="grid-column-start: 2;grid-column-end: 3;"></div>
<div class="grid-item-small" style="background-color: #e67664; grid-column-start: 3;grid-column-end: 4;">Contract (Salesforce, Zuora CPQ, DocuSign)</div>
<div class="grid-item-small" style="background-color: #e05842; grid-column-start: 4;grid-column-end: 5;">Fulfillment Engineers</div>
<div class="grid-item-empty" style="grid-column-start: 1;grid-column-end: 5;grid-row-start: 8;grid-row-end: 9"><i class="fas fa-arrow-down"></i></div>
<div class="grid-item-large" style="background-color: #d1d1f0; grid-row-start: 9;grid-row-end: 12; grid-column-start: 1;grid-column-end: 2;"><b>Bill</b></div>
<div class="grid-item-small" style="background-color: #a6a6de; grid-column-start: 2;grid-column-end: 4;">Zuora Subscription Lifecycle Management</div>
<div class="grid-item-small" style="background-color: #7c7ccc; grid-column-start: 4;grid-column-end: 5;">Billing</div>
<div class="grid-item-small" style="background-color: #a6a6de; grid-column-start: 2;grid-column-end: 4;">Zuora Billing</div>
<div class="grid-item-small" style="background-color: #7c7ccc; grid-column-start: 4;grid-column-end: 5;">Finance</div>
<div class="grid-item-small" style="background-color: #a6a6de; grid-column-start: 2;grid-column-end: 4;">Rev Rec (Netsuite)</div>
<div class="grid-item-small" style="background-color: #7c7ccc; grid-column-start: 4;grid-column-end: 5;">Enterprise Apps</div>
<div class="grid-item-empty" style="grid-column-start: 1;grid-column-end: 5;grid-row-start: 12;grid-row-end: 13"><i class="fas fa-arrow-down"></i></div>
<div class="grid-item-large" style="background-color: #73afea; grid-row-start: 13;grid-row-end: 14; grid-column-start: 1;grid-column-end: 2;"><b>Fulfill</b></div>
<div class="grid-item-small" style="background-color: #2e87e0; grid-column-start: 2;grid-column-end: 4;">License (Home Grown Application - Ruby on Rails)</div>
<div class="grid-item-small" style="background-color: #1f78d1; grid-column-start: 4;grid-column-end: 5;">Fulfillment Engineers</div>
<div class="grid-item-empty" style="grid-column-start: 1;grid-column-end: 5;grid-row-start: 14;grid-row-end: 15"><i class="fas fa-arrow-down"></i></div>
<div class="grid-item-large" style="background-color: #75d09b; grid-row-start: 15;grid-row-end: 18; grid-column-start: 1;grid-column-end: 2;"><b>Maintain</b></div>
<div class="grid-item-small" style="background-color: #37b96d; grid-column-start: 2;grid-column-end: 4;">Support (Zendesk)</div>
<div class="grid-item-small" style="background-color: #168f48; grid-column-start: 4;grid-column-end: 5;">Support Team</div>
<div class="grid-item-small" style="background-color: #37b96d; grid-column-start: 2;grid-column-end: 4;">Monitor (Home Grown Application "Version & SeatLink" - Ruby On Rails)</div>
<div class="grid-item-small" style="background-color: #168f48; grid-column-start: 4;grid-column-end: 5;">Fulfillment Engineers</div>
<div class="grid-item-small" style="background-color: #37b96d; grid-column-start: 2;grid-column-end: 3;">Digital Renewal</div>
<div class="grid-item-small" style="background-color: #37b96d; grid-column-start: 3;grid-column-end: 4;">Order Form Renewal (SFDC, Z-CPQ, DocuSign)</div>
<div class="grid-item-small" style="background-color: #168f48; grid-column-start: 4;grid-column-end: 5;">See Sell</div>
</div>
</div>

## Primary Enterprise Applications

#### Salesforce
* Salesforce is used as our CRM tool for managing customer Leads, Contacts, Accounts and Opportunities.
* Salesforce is owned by the Sales Systems team at GitLab.

#### Zuora Billing
* Zuora is used as our CPQ, Billing and revenue tool for managing customer subscriptions, payments and invoicing.
* Zuora is owned by the Finance team at GitLab.

#### Zuora Revenue
* Zuora Revenue is our automated revenue recognition application that meets current and future U.S. GAAP, including the new ASC 606 and IFRS 15 revenue standards.

#### CustomersDot
* CustomersDot is used when the customer logs in to manage their account details for their subscription
* GitLab engineers created CustomersDot and it is owned by the [Fulfillment team](/handbook/engineering/development/fulfillment/)

#### NetSuite
* NetSuite is the company Enterprise Resource Planning (ERP) system, which is primarily managed by the Finance team. 
* The platform allows enhanced dimensional reporting as well as multi-currency and multi-entity reporting. This is where the General Ledger resides and all financial activity is ultimately recorded, which is critical to reporting the financial health of the company.




## Resources

##### High Level

- [WIP: view only Lucidchart diagram lead to fulfillment system flow](https://app.lucidchart.com/documents/view/fe61ff48-c0e3-4f40-b2de-4023d48101d9/0_0)
- [video of custom setup](https://drive.google.com/drive/folders/1kfCEQM6XYGWYxq3Ke4TNvtmDR-46erVD)
- [Security's Compliance Diagram](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/blob/master/PCI/customers.gitlab.com_data_flow_diagram_-_New_Business.pdf)
- [Fulfillment Team's Web store and CustomersDot Flow Diagram](https://app.mural.co/t/gitlab2474/m/gitlab2474/1569500330861/8f9fd73826c42ad809d51be886db27494da91353)
- [Trade Compliance](/handbook/business-technology/trade-compliance/)
- [Sales flow](https://drive.google.com/file/d/1nkJrsXewy1G9llV9-8k2EhineU2hyoDJ/view?usp=sharing)
- [entry points, integration users google sheet](https://docs.google.com/spreadsheets/d/1j3xE6pQLfsKMri14LDcrnxbWbTwqz4Tpv9kI8UIHYCE/edit#gid=0)
- [PCI In-Scope Systems diagram](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/-/blob/master/PCI/PCI_In_Scope_Systems.md)
- [entry points and conversion by marketing](https://www.figma.com/file/JMFCXAftW30wjul6TTIPFa/lead%2Fsign-up-flow?node-id=0%3A1)
- [customer facing documentation on managing subscriptions](https://docs.gitlab.com/ee/subscriptions/)
- [UX flows for trials](https://app.mural.co/t/gitlab2474/m/gitlab2474/1580984258623/a815e52decef6141307b634da65fbcd5242a48e8)

##### Self-managed

- [renewal UX flow](https://gitlab.com/gitlab-org/growth/ui-ux/-/issues/75)
- [Usage ping on Self-managed](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html#request-flow-example-1)
- [Self-managed purchase flow video](https://www.youtube.com/watch?v=y_j1ini0qrA)

##### SaaS

- [SaaS renewal flow diagram](https://whimsical.com/S4r7nuRFgQ5kZKaZisZcoQ)
- [SaaS purchase flow video/customer app](https://www.youtube.com/watch?v=p32PRZEzhg0)
- [trial SaaS video](https://gitlab.zoom.us/recording/play/sGNG2-Ny2wIr4tsWcPHNWaoDDyC-EkSvKJEBeAO54Mz4KhF0pw9Kn66OrBcjJHms?continueMode=true)

##### CI minutes

- [walkthrough video of purchasing CI minutes](https://drive.google.com/drive/folders/1UxEVNgAkL2TwH8NOOmT4a6cFL_vqIZCH)


# Process Flow Diagrams



##### Sales-Assisted: New Subscription


<div class="center">
<div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://lucid.app/documents/embeddedchart/0f691079-8ee9-4663-84d7-6d03e7f87ed8" id="PNs5p_JZW4yg"></iframe></div>
</div>




##### DRAFT: Sales-Assisted: Renewal


<div class="center">
<div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://app.lucidchart.com/documents/embeddedchart/5845e3b1-d8fc-453c-984f-3c66b1dbea3d" id="4b-y4oHm3itk"></iframe></div>
</div>

##### Web Direct: New Subscription
<div class="center">
<div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://lucid.app/documents/embeddedchart/09ac03f9-ae40-4bbd-961c-f64bb44f5b4f" id="KWs5ZPs-THoH"></iframe></div>
</div>

##### Lead-to-Fulfillment System Architecture
<div class="center">
<div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://app.lucidchart.com/documents/embeddedchart/b5a0e4b3-ff40-4685-8d21-3109696c8f64" id="X~9ywPrrncS-"></iframe></div>
</div>

##### Zuora and Salesforce
<img src="/handbook/business-technology/images/zuoraSFDC1.png" class="full-width">

## System Integrations 
- API should send country codes ISO 2 from CustomersDot or GitLab.com forms to SFDC and Marketo
- [Required fields](https://gitlab.com/groups/gitlab-org/-/epics/3627) for submitting new leads to Marketo in order for them to be properly rotated to the sales team.

# Zuora Documentation
- [overall Zuora system configurations](https://docs.google.com/spreadsheets/d/11VnuTlWuMe6XGf6gB0_x16MSQvJryuEY8gQRx66wjJ4/edit?ts=5f0f28d0#gid=152692183)
- [finance systems integrations inventtory](https://docs.google.com/spreadsheets/d/1H2DTuP0lS6J129pProMajhdH_as6i38RV1_FjA59uM8/edit#gid=0)
- [heatmap of finance systems](https://app.lucidchart.com/invitations/accept/df465e7e-de32-4e6f-b93d-0c89044bca5c)

<details>
<summary markdown='span'>
  Zuora Communication Profiles
</summary>

- Communication Profiles:
    - New accounts are set to `Default Profile`
    - `Silent Profile` is used temporarily when the customer should not receive notifications.
        _note: this would prevent the system from making the license available in the CustomersDot._

</details>

<details>
<summary markdown='span'>
  Zuora: Billing Batches
</summary>

Billing batches allow for account segmentation within Zuora based on billing processes.  Batch1 is used for all Web Direct clients who purchase online.  Batch2 is used for all standard, Sales Assisted accounts which can be billed without manual intervention. This Batch includes resellers for which we bill in a standard way.  Batch3 is used for Non-Standard Sales Assisted accounts, which are billed individually and require manual intervention by the Billing Team for various reasons. Batch 50 is used for EDU / OSS clients.  

</details>

<details>
<summary markdown='span'>
  Zuora SKUs
</summary>

- [Product list in Zuora](https://docs.google.com/spreadsheets/d/1CtYcjutbuZ8FrxMN0BdVXSc8nWahjhcgoq2Gmb0R-xc/edit#gid=0)

</details>

<details>
<summary markdown='span'>
  Zuora Entities
</summary>

- All web direct accounts are sent to GitLab, Inc. (US) entity for initial purchase.
    If a deal moves to sales assisted for any reason, we then create an account manually on the correct entity if it differs from US.

The defaults are related to the address delineating the entity based on the location of the customer.
Country specification is unrelated to a close process.
Note that the GitLab entity information will be populated via the following rules.
This table is based on the ISO-2 billing country code of the direct customer or reseller we are delivering invoices to:

| Entity               | Direct/Unauthorized Reseller | Authorized Reseller   |
| -------------------- | ---------------------------- | --------------------- |
| BV (Netherlands)     | NL                           | Not AU, DE, UK, or US |
| GmbH (Germany)       | DE                           | DE                    |
| Ltd (United Kingdom) | UK                           | UK                    |
| Inc (United States)  | Not AU, NL, DE, or US        | US                    |
| Pty Ltd (Australia)  | Not NL, DE, UK, or US        | AU                    |

For example:
A sales assisted customer or unauthorized reseller in the Netherlands (NL) will be billed out of GitLab BV; Germany from GitLab GmbH; United Kingdom from GitLab Ltd; any sales assisted customer outside these countries will be billed from GitLab Inc.
An authorized reseller based on Germany will be billed from GitLab GmbH; United Kingdom from GitLab Ltd; United States from GitLab Inc; any resellers based outside these countries will be billed out of GitLab BV.

</details>

### Notifications

<details>
<summary markdown='span'>
  renewal mailings
</summary>

This flow diagram shows the current renewal email cadence, depending on auto-renew settings.
Note: Email content cannot currently be tailored to SaaS vs. Self-managed.
<img src="/images/growth/renewal_emails.png">

- The main difference between the flows is whether auto-renew is on or not
- The emails come from Zuora, with the exception of the "14 day SaaS auto-renew email", which is sent by the CustomersDot
    - 14 day SaaS auto-renew email is only sent if there is an increase in the customer's seat count which will lead to an increased bill

#### 90 day email

<a href="/images/growth/renewal_mailing_90.png"><img src="/images/growth/renewal_mailing_90.png"></a>

#### 45 day email

<a href="/images/growth/renewal_mailing_45.png"><img src="/images/growth/renewal_mailing_45.png"></a>

#### 21 day email (auto-renew on)

<a href="/images/growth/renewal_mailing_21_3.png"><img src="/images/growth/renewal_mailing_21_3.png"></a>

#### 15 day email

<a href="/images/growth/renewal_mailing_15.png"><img src="/images/growth/renewal_mailing_15.png"></a>

#### 14 day email (auto-renew on and having an increase)

<a href="/images/growth/renewal_mailing_14.png"><img src="/images/growth/renewal_mailing_14.png"></a>

#### 3 day email (auto-renew on)

<a href="/images/growth/renewal_mailing_21_3.png"><img src="/images/growth/renewal_mailing_21_3.png"></a>

#### 0 day email

<a href="/images/growth/renewal_mailing_0.png"><img src="/images/growth/renewal_mailing_0.png"></a>

</details>

<details>
<summary markdown='span'>
  customer application emails
</summary>

Emails notifications are sent through Mailgun

- Reseller Notification: this email is delivered from the CustomersDot after the customer accepts the EULA.
    - [HAML/HTML](https://gitlab.com/gitlab-org/customers-gitlab-com/blob/5e50769b1ef64f9002ecf9b59dbba5cc24031733/app/views/customer_mailer/reseller_notification_license_sent.html.haml#L1)
    - [Text](https://gitlab.com/gitlab-org/customers-gitlab-com/blob/5e50769b1ef64f9002ecf9b59dbba5cc24031733/app/views/customer_mailer/reseller_notification_license_sent.text.erb#L3)
- Customer - Accept Terms Request: this email is delivered from the CustomersDot when the EULA is required to be accepted before delivering the license.
    - [HAML/HTML](https://gitlab.com/gitlab-org/customers-gitlab-com/blob/3248ac5978678b6920d7cb755be288e312fda8aa/app/views/customer_mailer/accept_terms_request.html.haml#L1)
    - [Text](https://gitlab.com/gitlab-org/customers-gitlab-com/blob/3248ac5978678b6920d7cb755be288e312fda8aa/app/views/customer_mailer/accept_terms_request.text.erb#L4)
- Customer - License Email: this email is delivered from LicenseDot after the license is created following the EULA acceptance.
    - [HAML/HTML](https://gitlab.com/gitlab-org/license-gitlab-com/blob/12dffa87b8b1c092b7abebc63a7aaae61528f68f/app/views/customer_mailer/license_email.html.haml#L1)
    - [Text](https://gitlab.com/gitlab-org/license-gitlab-com/blob/f8e7e146b10319640176e1cedf395c5a83c325bd/app/views/customer_mailer/license_email.text.erb#L1)

</details>

### Definitions

<details>
<summary markdown='span'>
  subscriptions
</summary>

`Start Date` (Subscription): the start date of this transaction

- `Subscription Term Start Date`: the start date of the subscription itself
- [`active_user_count`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/usage_data.rb#L32): is pulling any user who is not blocked or deactivated, used in usage ping
- [`current_active_users_count`](https://gitlab.com/gitlab-org/gitlab/blob/master/ee/app/models/historical_data.rb#L15): the active user count applicable for billing purposes and captured in the daily cron job. This is labeled `billable_users` in usage ping. This value does not count the users which are not applicable to billing:
    - GitLab generated service bots
    - Guest users in Ultimate subscriptions

</details>

<details>
<summary markdown='span'>
  Self-managed EE users
</summary>

- a license holder is someone who pays for a GitLab license and manages the payments for a Self-managed installation.
[EULA](https://customers.gitlab.com/admin/eula), [TOU](https://about.gitlab.com/terms), [FOSS LICENSE](https://gitlab.com/gitlab-org/gitlab/blob/master/LICENSE), [EE LICENSE](https://gitlab.com/gitlab-org/gitlab/blob/master/ee/LICENSE)
- a Self-managed admin is someone who manages the installation and users of a Self-managed installation.
[FOSS LICENSE](https://gitlab.com/gitlab-org/gitlab/blob/master/LICENSE), <https://gitlab.com/gitlab-org/gitlab/blob/master/ee/LICENSE>
- a Self-managed user is someone who uses a Self-managed installation.
[FOSS LICENSE](https://gitlab.com/gitlab-org/gitlab/blob/master/LICENSE), <https://gitlab.com/gitlab-org/gitlab/blob/master/ee/LICENSE>

</details>

<details>
<summary markdown='span'>
  dotcom
</summary>

- a dotcom subscriber is someone who pays for dotcom.
[EULA](https://customers.gitlab.com/admin/eula), [TOU](https://about.gitlab.com/terms)
- a dotcom user are people who can be assigned to seats that a dotcom subscriber is paying for.
[TOU](https://about.gitlab.com/terms)

</details>

<details>
<summary markdown='span'>
  Self-managed FOSS
</summary>

- a Self-managed admin is someone who manages the installation and users of a Self-managed installation.
[FOSS LICENSE](https://gitlab.com/gitlab-org/gitlab/blob/master/LICENSE)
- a Self-managed user is someone who uses a Self-managed installation.
[FOSS LICENSE](https://gitlab.com/gitlab-org/gitlab/blob/master/LICENSE)

</details>

## Change Management

<details>
<summary markdown='span'>
  Adding new SKUs
</summary>

- When a new SKU is added, the Fulfillment team [must be notified thirty (30) days in advance](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/new?issuable_template=Sku) with the requirements so that they can prepare the customer applications.

</details>

<details>
<summary markdown='span'>
  SFDC Sandbox Refresh
</summary>

- When the SFDC sandbox is refreshed, the Fulfillment team is notified and the SFDC sandbox is reintegrated with the Zuora sandbox.

</details>

<details>
<summary markdown='span'>
  History of Change
</summary>

- [google doc](https://docs.google.com/document/d/1hWL7qJ2K0_1UH4CjNIyAWmcEVH6nlVf0svsDUxU89lk/edit)

</details>

## Customer Success and Billing

##### related resources: licensing, billing, transactions

- [Issue: Analysis: Licenses & Terms](https://gitlab.com/gitlab-com/business-technology/enterprise-apps/bsa/portal-analysis/-/issues/34)
- [Troubleshooting: True Ups, Licenses + EULAs](/handbook/business-technology/enterprise-applications/applications/troubleshooting/)
- [Troubleshooting subscription and licensing problems](/handbook/support/license-and-renewals/workflows/license_troubleshooting.html)
- [License documentation](https://docs.gitlab.com/ee/user/admin_area/license.html)
- [CustomersDot: Admin internal documentation](/handbook/internal-docs/customers-admin/)


## Licensing and Renewals (L&R) Queue in Zendesk

The CustomersDot intersects with Support, Billing, Sales, and Product primarily.
Tickets come into the L&R queue in Zendesk generally three ways, generally prioritized as low:

- by [opening a support ticket](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=334487) and selecting `Help with my license`

- by filling out the form on the [Renewals page](https://about.gitlab.com/renewals/)
    The L&R Queue is a separate queue of Zendesk tickets that fall outside of the scope of customer product support; it's handled by Support by triaging the issue and rerouting the tickets to the appropriate team.

- **Passing to Sales:** Pass tickets relating to IACV to Sales via cc to Sales in reply to ticket which sends an email to both customer and account owner. If an account is associated with the ticket, there's a ticket object in SFDC.
    It's then Sales's responsibility to continue the customer interaction and the BSS closes the Zendesk ticket.

- **Passing to Sales:** If a ticket comes in and Support is unable to determine the account owner or the inquiry is related to new business, they reach out to the SFDC chatter group `ZD-newbusiness` where the SDR Regional leads for EMEA, APAC and AMER/LATAM provide an answer.
    Then Support cc's that person on the ticket and closes in Zendesk.
        - Regional Leads for SDRs:
            - AMER/LATAM - Mona Elliot
            - APAC - Jay Thomas-Burrows
            - EMEA - Elsje Smart

- **Passing to Billing:** The mechanism to pass tickets to the Billing team is to open the ticket, then change the field `form` from the value `Upgrades, Renewals & AR (refunds)` to `Accounts Receivable/Refunds`.
    This puts the ticket into the Billing Team's queue which they then manage.


## Contact Us
### Slack Channels
- `#enterprise-apps`
- `#business-technology`
- `#bt-finance-operations`
- `#financesystems_help`
- `#bt-integrations`

### GitLab Issues
<div class="flex-row" markdown="0" style="height:80px">
  <a href="https://gitlab.com/gitlab-com/business-technology/business-technology/-/issues/new" class="btn btn-purple-inv" style="width:33%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Business Systems Analysts</a>
  <a href="https://gitlab.com/gitlab-com/business-technology/enterprise-apps/integrations/issue-tracker/-/issues/new" class="btn btn-purple-inv" style="width:33%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Integrations Engineering</a>
  <a href="https://gitlab.com/gitlab-com/business-technology/enterprise-apps/financeops/finance-systems" class="btn btn-purple-inv" style="width:33%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Finance Systems</a>
