---
layout: markdown_page
title: "Category Direction - Gitaly AuthX"
description: ""
canonical_path: "/direction/create/gitaly_authx/"
---

- TOC
{:toc}

## Gitaly AuthX

The current design of authentication and authorization causes scalability problems for all mutating Git operations. Dependending on repository size, users may need to wait for dozens of seconds until a tiny change has been accepted, which can be frustrating and conveys a general sense of GitLab being inperformant. By focussing on a redesign of these access checks, we can bring down the latency and thus increase both performance and throughput.

### Investment Allocation

Currently, we are staffing this Category with 15% of the Gitaly team output, or equivalent to ~1 engineer.

## Additional Resources

- [Epic](https://gitlab.com/groups/gitlab-org/-/epics/5717)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=gitaly%3A%3Aauthx)
- [Product Direction Page](/direction/create/gitaly/)

## What's Next & Why

To begin working towards our goal, we will focus on the following topics:

|   | Topic     | Epic           | Note |
|---|-----------|----------------|------|
| 1 | Create measurable baseline for Gitaly AuthX checks | https://gitlab.com/groups/gitlab-org/-/epics/6137 | Requirement to measure success and define an exit criteria |
| 2 | Provide more efficient interfaces to perform Git authentication checks | https://gitlab.com/groups/gitlab-org/-/epics/6138 | Existing interfaces are often a bad fit and/or invoked with inefficient patterns |
| 3 | Improve latency on Git access check interfaces | https://gitlab.com/groups/gitlab-org/-/epics/6139 | Implementations of interfaces used for AuthX are inefficient at times and can be improved |

###  Vision Items

Our top vision items we have defined include:

1. [Move AuthX into Gitaly](https://gitlab.com/groups/gitlab-org/-/epics/5985)
